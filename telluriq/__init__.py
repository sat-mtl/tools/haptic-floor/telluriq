"""An experimental client to monitor and control haptic floors"""

from kivy import resources
import os

# adds a custom path to search in
resources.resource_add_path(os.path.dirname(__file__))

from .app import Telluriq

__version__ = "0.2.0"

app = Telluriq()
