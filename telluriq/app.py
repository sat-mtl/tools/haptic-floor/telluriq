from .listeners import KivyOSCServiceListener
from .osc import OSCMultiClient
from .widgets import Console, ConsoleView, HelpButton, CalibrateButton, SettingsButton
import kivy
from kivy.uix.screenmanager import ScreenManager, Screen
from zeroconf import Zeroconf
from kivymd.app import MDApp
from kivymd.uix.datatables import MDDataTable
from kivy.metrics import dp
from kivy.uix.boxlayout import BoxLayout
from kivy.modules import inspector
from kivy.core.window import Window


class UserInterface(ScreenManager):
    pass


class Telluriq(MDApp):

    osc = OSCMultiClient()

    def get_application_config(self):
        """returns absolute path to `.ini` file"""
        return None  # avoid ini file

    def build_config(self, config):
        config.setdefaults(
            "kivy",
            {
                "default_font": [
                    "Inter",
                    "assets/fonts/Inter/Inter-Regular.otf",
                    "assets/fonts/Inter/Inter-Italic.otf",
                    "assets/fonts/Inter/Inter-Bold.otf",
                    "assets/fonts/Inter/Inter-BoldItalic.otf",
                ]
            },
        )
        # override graphics section
        config.setdefaults("graphics", {"resizable": 1, "width": 375, "height": 557})

        # update our app config with default config
        config.update_config(kivy.config.Config.filename)

    def build(self):
        # set kivy config instance as our app config
        kivy.config.Config = self.config
        self.title = self.__class__.__name__
        self.icon = "assets/logo_alt.png"
        # set default theme
        self.theme_cls.theme_style = "Dark"
        root = UserInterface()
        inspector.create_inspector(Window, root)
        # return root widget
        return root

    def on_start(self):
        screen = self.root.ids.nodes.ids.display.screens[1]

        registry = MDDataTable(
            size_hint=(1.0, 1.0),
            use_pagination=True,
            check=True,
            column_data=[
                ("Name", dp(30)),
                ("Server", dp(30)),
                ("Address", dp(30)),
                ("Port", dp(15)),
                ("UUID", dp(70)),
                ("Type", dp(30)),
            ],
        )

        # self.osc.send("/logs/activate", 9001)

        screen.add_widget(registry)

        zeroconf = Zeroconf()
        zeroconf.add_service_listener(
            "_osc._udp.local.", KivyOSCServiceListener(self.osc, registry)
        )

    def on_stop(self):
        self.root.ids.console.server_thread.stop()
