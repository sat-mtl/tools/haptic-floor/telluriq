# Telluriq

Telluriq is an experimental *graphical user interface* that monitors and calibrates haptic floors.
It provides an easy way to manipulate a range of [D-Box](https://www.d-box.com) actuators in order to prepare experimental performances with the haptic technology.

## Development
### Prerequisites
#### Install required tools
All these tools are required before doing anything with this code base.

| Name                                                                 | Version  | Description                                    |
|----------------------------------------------------------------------|----------|------------------------------------------------|
| [Python](https://www.python.org/)                                    | ≥ 3.8    | Required to execute Telluriq source            |
| [pip](https://pypi.org/project/pip/)                                 | ≥ 21.0   | Recommended tool that installs dependencies    |
| [Virtualenv](https://virtualenv.pypa.io/en/latest/)                  | ≥ 20.10  | Recommended tool that isolates GUI development |
| [Black](https://github.com/psf/black)                                | ≥ 22.1.0 | Recommended tool that format Python code       |
| [flake8](https://github.com/pycqa/flake8)                            | ≥ 4.0.1  | Recommended tool that lint Python code         |
| [Kivy](https://kivy.org/doc/stable/gettingstarted/installation.html) | ≥ 2.0.0  | Required GUI toolkit                           |
| [pyOSC3](https://github.com/Qirky/pyOSC3)                            | ≥ 1.2    | Required OpenSoundControl library              |
| [zeroconf](https://github.com/jstasiak/python-zeroconf)              | ≥ 0.38.4 | Required ZeroConf library                      |


#### Use the make scripts
All the [make](https://www.gnu.org/software/make) tasks centralize the development/maintenance tasks.

| Command             | Step        | Purpose       | Description                                                                    |
|---------------------|-------------|---------------|--------------------------------------------------------------------------------|
| `make setup-dev`    |             | Development   | Install the dev dependencies from requirement-dev.txt using pip                |
| `make setup-doc`    |             | Documentation | Install the doc dependencies                                                   |
| `make install`      |             | Development   | Install the runtime dependencies from pyproject.toml using flit                |
| `make init`         | Environment | Development   | Initialize the [virtualenv](https://virtualenv.pypa.io/en/latest/) environment |
| `make build`        | Packaging   | Development   | Build the application                                                          |
| `make doc`          |             | Documentation |                                                                                |
| `make lint`         | Linting     | Development   | Lint the code base with [flake8](https://flake8.pycqa.org/en/latest/)          |
| `make format`       |             | Development   | Format the code base with [black](https://black.readthedocs.io/en/stable/)     |
| `make check-format` |             | Development   | Check the code format (without reformatting)                                   |
| `make start`        | Testing     | Development   | Start the application                                                          |
| `make watch`        | Testing     | Development   | Restart the application when source code files are modified (uses entr)        |
| `make serve-doc`    |             | Documentation | Serve the documentation website on localhost:8000                              |


### Setup development environment from source

Following this quick start guide will setup Telluriq development environment on Xubuntu 20.04.

Using Kivy's recommended approach, dependencies are downloaded as pre-compiled binary _wheel_ packages from PyPI repository into a Python virtual environment located in env directory.

#### Install system dependencies

```bash
sudo apt update
sudo apt install -y \
    python3-pip \
    git \
    entr \
    xclip # https://github.com/kivy/kivy/issues/4036
```

#### Install setuptools and virtualenv from pip

```bash
python3 -m pip install --user --upgrade pip virtualenv
```

#### Clone repository

Change develop to another branch name you'd like to test.

```bash
git clone -b develop https://gitlab.com/sat-mtl/telepresence/telluriq.git
cd telluriq
```

#### Setup Virtual Environment

Create a new python virtual environment using the built-in `venv` module:

```bash
make init
```

#### Enter Virtual Environment

We are now able to activate the virtual environment by sourcing the activate script:

```bash
. activate
```

#### Install development dependencies

We can install required developpement dependencies:

```bash
make setup-dev
```

#### Install runtime dependencies

We can install required runtime dependencies:

```bash
make install
```

#### Start the application

Whenever your virtual environment is active, starting the application is as easy as:

```bash
make start
```

#### Exit Virtual Environment

Note that once done, we can deactivate the virtual environment by issuing the following function sourced by the environment:

```bash
deactivate
```

### Contributing
#### Using git hooks
Some commands like _linting_ and _formating_ should be executed with a git hook.

SHOULD BE TESTED!

```bash
cat << EOF > .git/hooks/pre_commit
make format
make lint
EOF
```

## Licenses

According to its license, [Inter](https://rsms.me/inter) font family is bundled in this package:

+ [Inter](assets/fonts/inter/LICENSE.txt)
